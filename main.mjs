import * as utils from 'test-utils';
import getUtils from 'util';

const params = { password: 'sdf234fdsf32cdsc' };

const myBestFunc = getUtils.promisify(utils.runMePlease);

try {
  const result = await myBestFunc(params);
  console.log(result);
} catch (error) {
  console.log(error);
}